package br.com.builders.desafio.exception;

import br.com.builders.desafio.rest.CustomerController;
import org.junit.Test;
import org.springframework.core.MethodParameter;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultExceptionHandlerTest {

    private DefaultExceptionHandler defaultExceptionHandler = new DefaultExceptionHandler();

    @Test
    public void handleBadRequestTest() {
        final Method method = CustomerController.class.getDeclaredMethods()[0];
        final MethodParameter methodParameter = new MethodParameter(method, 0);
        final DirectFieldBindingResult bindingResult = new DirectFieldBindingResult(this, "");
        bindingResult.addError(new FieldError("Test", "field", "Contains error!"));
        final MethodArgumentNotValidException exception = new MethodArgumentNotValidException(methodParameter, bindingResult);
        final ResponseEntity<ErrorMessage> result = defaultExceptionHandler.handleBadRequest(exception);
        assertThat(result.getBody().getMessage()).isEqualTo("The request is not valid!");
        assertThat(result.getBody().getErrors()).contains("field: Contains error!");
    }

    @Test
    public void handleRuntimeExceptionTest() {
        final ResponseEntity<ErrorMessage> result = defaultExceptionHandler.handleRuntimeException(new RuntimeException("Test"));

        assertThat(result.getBody().getMessage()).isEqualTo("Test");
    }

    @Test
    public void handleRuntimeExceptionWithResponseStatusTest() {
        final ResponseEntity<ErrorMessage> result = defaultExceptionHandler.handleRuntimeException(new NotFoundException("Test"));

        assertThat(result.getBody().getMessage()).isEqualTo("Test");
    }

}
