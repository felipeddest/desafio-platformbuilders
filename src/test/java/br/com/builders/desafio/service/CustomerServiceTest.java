package br.com.builders.desafio.service;

import br.com.builders.desafio.domain.Customer;
import br.com.builders.desafio.exception.NotFoundException;
import br.com.builders.desafio.repository.CustomerRepository;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @InjectMocks
    private CustomerService service;

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    private CustomerRepository repository;

    @Mock
    private MongoConverter mongoConverter;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @After
    public void after() {
        Mockito.reset(mongoTemplate, repository, mongoConverter);
    }

    @Test
    public void getAllTest() {
        when(repository.findAll()).thenReturn(singletonList(Customer.builder().build()));

        List<Customer> customerList = service.getAll();

        assertThat(customerList).hasSize(1);
    }

    @Test
    public void createCustomerTest() {
        final Customer customer = Customer.builder()
                .id("asd")
                .name("test")
                .login("test@test.com")
                .baseUrl("www.test.com")
                .crmId("C123").build();

        when(mongoTemplate.exists(any(Query.class), any(Class.class))).thenReturn(false);
        when(repository.save(any(Customer.class))).thenReturn(customer);

        service.createCustomer(customer);

        verify(mongoTemplate).exists(any(Query.class), any(Class.class));
        verify(repository).save(any(Customer.class));
    }

    @Test
    public void getCustomerTest() {
        final Customer customer = Customer.builder()
                .id("asd")
                .name("test")
                .login("test@test.com")
                .baseUrl("www.test.com")
                .crmId("C123").build();

        when(repository.findOne(anyString())).thenReturn(customer);

        final Customer result = service.getCustomer("asd");

        assertThat(customer).isEqualTo(result);
    }

    @Test
    public void getCustomerNotFoundTest() {
        when(repository.findOne(anyString())).thenReturn(null);

        thrown.expect(NotFoundException.class);

        service.getCustomer("asd");

        assertThat(thrown).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void updateCustomerTest() {

        final Customer customer = Customer.builder()
                .id("asd")
                .name("test")
                .login("test@test.com")
                .baseUrl("www.test.com")
                .crmId("C123").build();

        when(mongoTemplate.exists(any(Query.class), any(Class.class))).thenReturn(true);

        when(mongoTemplate.findAndModify(any(Query.class), any(Update.class), any(Class.class))).thenReturn(customer);

        service.updateCustomer("asd", customer);

        verify(mongoTemplate).findAndModify(any(Query.class), any(Update.class), any(Class.class));

    }

    @Test
    public void updateCustomerNotFoundTest() {

        final Customer customer = Customer.builder()
                .id("asd")
                .name("test")
                .login("test@test.com")
                .baseUrl("www.test.com")
                .crmId("C123").build();

        thrown.expect(NotFoundException.class);

        when(mongoTemplate.exists(any(Query.class), any(Class.class))).thenReturn(false);

        service.updateCustomer("asd", customer);

        assertThat(thrown).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void deleteCustomerTest() {

        doNothing().when(repository).delete(anyString());

        when(mongoTemplate.exists(any(Query.class), any(Class.class))).thenReturn(true);

        service.deleteCustomer("asd");

        verify(repository).delete("asd");
    }

    @Test
    public void deleteCustomerNotFoundTest() {

        thrown.expect(NotFoundException.class);

        when(mongoTemplate.exists(any(Query.class), any(Class.class))).thenReturn(false);

        service.deleteCustomer("asd");

        assertThat(thrown).isInstanceOf(NotFoundException.class);
    }



}
