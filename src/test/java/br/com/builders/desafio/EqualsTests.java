package br.com.builders.desafio;

import br.com.builders.desafio.domain.Customer;
import br.com.builders.desafio.dto.request.CreateOrReplaceCustomerRequest;
import br.com.builders.desafio.dto.request.ModifyCustomerRequest;
import br.com.builders.desafio.dto.response.CustomerResponse;
import br.com.builders.desafio.dto.response.DetailedCustomerResponse;
import br.com.builders.desafio.exception.ErrorMessage;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import static nl.jqno.equalsverifier.Warning.NONFINAL_FIELDS;
import static nl.jqno.equalsverifier.Warning.STRICT_INHERITANCE;

public class EqualsTests {

    @Test
    public void detailedCustomerResponseEquals() {
        EqualsVerifier.forClass(DetailedCustomerResponse.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

    @Test
    public void modifyCustomerRequestEquals() {
        EqualsVerifier.forClass(ModifyCustomerRequest.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

    @Test
    public void customerResponseEquals() {
        EqualsVerifier.forClass(CustomerResponse.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

    @Test
    public void createOrReplaceCustomerEquals() {
        EqualsVerifier.forClass(CreateOrReplaceCustomerRequest.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

    @Test
    public void customerEquals() {
        EqualsVerifier.forClass(Customer.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

    @Test
    public void errorMessageEquals() {
        EqualsVerifier.forClass(ErrorMessage.class).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS).verify();
    }

}
