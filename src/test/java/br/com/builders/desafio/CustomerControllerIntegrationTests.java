package br.com.builders.desafio;

import br.com.builders.desafio.config.MongoCleanupRule;
import br.com.builders.desafio.domain.Customer;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerControllerIntegrationTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Rule
    public MongoCleanupRule rule = new MongoCleanupRule(this, Customer.class);

    @Before
    public void prepare() {
        mockMvc = webAppContextSetup(context).build();
    }

    @Test
    public void shouldCreateCustomer() throws Exception {
        mockMvc.perform(post("/customers")
                .content(readFile("jsons/request_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReceiveBadRequestIfInvalidOnCreateCustomer() throws Exception {
        mockMvc.perform(post("/customers")
                .content(readFile("jsons/request_invalid_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReceiveConflictIfCreateCustomerWithSameLogin() throws Exception {
        mongoTemplate.save(createCustomer("test@test.com", "C1"));

        mockMvc.perform(post("/customers")
                .content(readFile("jsons/request_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict());
    }

    @Test
    public void shouldGetAllCustomers() throws Exception {
        mongoTemplate.save(createCustomer("felipe@test.com", "C1"));
        mongoTemplate.save(createCustomer("tasca@test.com", "C2"));

        mockMvc.perform(get("/customers")
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.[*].crmId", containsInAnyOrder("C1", "C2")));
    }

    @Test
    public void shouldGetCustomer() throws Exception {
        final Customer customer = createCustomer("felipe@test.com", "C1");
        mongoTemplate.save(customer);

        mockMvc.perform(get("/customers/" + customer.getId())
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(customer.getId())))
                .andExpect(jsonPath("$.login", equalTo("felipe@test.com")))
                .andExpect(jsonPath("$.name", equalTo("Test")))
                .andExpect(jsonPath("$.crmId", equalTo("C1")))
                .andExpect(jsonPath("$.baseUrl", equalTo("www.test.com.br")));
    }

    @Test
    public void shouldReceiveNotFoundWhenCustomerDoesNotExists() throws Exception {
        mockMvc.perform(get("/customers/25297e8e-ea9f-4108-adb5-2a5b833141b6")
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReplaceACustomer() throws Exception {
        final Customer customer = createCustomer("felipe@test.com", "C1");

        mongoTemplate.save(customer);

        mockMvc.perform(put("/customers/" + customer.getId())
                .content(readFile("jsons/request_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReceiveNotFoundWhenReplacingACustomerThatNotExists() throws Exception {
        mockMvc.perform(put("/customers/25297e8e-ea9f-4108-adb5-2a5b833141b6")
                .content(readFile("jsons/request_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReceiveBadRequestWhenReplacingACustomerWithInvalidRequest() throws Exception {
        mockMvc.perform(put("/customers/25297e8e-ea9f-4108-adb5-2a5b833141b6")
                .content(readFile("jsons/request_invalid_create_or_replace_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldModifyCustomer() throws Exception {
        final Customer customer = createCustomer("felipe@test.com", "C1");

        mongoTemplate.save(customer);

        mockMvc.perform(patch("/customers/" + customer.getId())
                .content(readFile("jsons/request_modify_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

        mockMvc.perform(get("/customers/" + customer.getId())
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(customer.getId())))
                .andExpect(jsonPath("$.login", equalTo("felipe@test.com")))
                .andExpect(jsonPath("$.name", equalTo("Modified Name")))
                .andExpect(jsonPath("$.crmId", equalTo("C1")))
                .andExpect(jsonPath("$.baseUrl", equalTo("www.test.com.br")));
    }

    @Test
    public void shouldReceiveBadRequestWhenModifyRequestIsInvalid() throws Exception {
        final Customer customer = createCustomer("felipe@test.com", "C1");

        mongoTemplate.save(customer);

        mockMvc.perform(patch("/customers/" + customer.getId())
                .content(readFile("jsons/request_invalid_modify_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReceiveNotFoundWhenModifyingACustomerThatNotExists() throws Exception {
        mockMvc.perform(patch("/customers/25297e8e-ea9f-4108-adb5-2a5b833141b6")
                .content(readFile("jsons/request_modify_customer.json"))
                .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeleteACustomer() throws Exception {
        final Customer customer = createCustomer("felipe@test.com", "C1");

        mongoTemplate.save(customer);

        mockMvc.perform(delete("/customers/" + customer.getId()))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReceiveNotFoundWhenDeletingACustomerThatNotExists() throws Exception {
        mockMvc.perform(delete("/customers/25297e8e-ea9f-4108-adb5-2a5b833141b6"))
                .andExpect(status().isNotFound());
    }

    private Customer createCustomer(final String login, final String crmId) {
        return Customer.builder()
                .id(UUID.randomUUID().toString())
                .baseUrl("www.test.com.br")
                .crmId(crmId)
                .login(login)
                .name("Test")
                .build();
    }

    private String readFile(String path) throws IOException {
        return FileUtils.readFileToString(new File(getClass().getResource(String.format("/%s", path)).getFile()), UTF_8);
    }


}
