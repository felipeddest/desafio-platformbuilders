/*
 * Copyright 2018 Builders
 *************************************************************
 *Nome     : ChallengeApplication.java
 *Autor    : Builders
 *Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
 *Empresa  : Platform Builders
 *************************************************************
 */
package br.com.builders.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The class <code>ChallengeApplication</code> start up the application.
 *
 * @author Felipe Tasca
 */
@EnableSwagger2
@SpringBootApplication
@ComponentScan(basePackages = {"br.com.builders.desafio"})
public class ChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallengeApplication.class, args);
    }
}
