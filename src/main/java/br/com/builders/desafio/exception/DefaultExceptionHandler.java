package br.com.builders.desafio.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * This Exception Handler will handle all exceptions thrown by the controllers.
 * <p/>
 * If the exception is annotated with @ResponseStatus, the HTTP Status informed there will be used to the response.
 * <p/>
 * This class also handle exceptions of bean validations so a message will be prepared with each validation error, and the status will be Bad Request.
 *
 * @author Felipe Tasca
 */
@ControllerAdvice
public class DefaultExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleBadRequest(final MethodArgumentNotValidException ex) {
        final List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        return new ResponseEntity<>(new ErrorMessage(HttpStatus.BAD_REQUEST.value(), "The request is not valid!", errors),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorMessage> handleRuntimeException(final Throwable exception) {
        final Class<? extends Throwable> clazz = exception.getClass();

        if (clazz.isAnnotationPresent(ResponseStatus.class)) {
            final HttpStatus httpStatus = getHttpStatus(clazz);
            logException(httpStatus, exception);
            return new ResponseEntity<>(new ErrorMessage(httpStatus.value(), exception.getMessage()), httpStatus);
        }

        logException(HttpStatus.INTERNAL_SERVER_ERROR, exception);
        return new ResponseEntity<>(new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void logException(final HttpStatus httpStatus, final Throwable exception) {
        logger.error("Error code " + httpStatus.toString());
        logger.error("Cause: " + exception.getMessage(), exception);
    }

    private HttpStatus getHttpStatus(final Class<? extends Throwable> clazz) {
        return clazz.getAnnotation(ResponseStatus.class).value();
    }
}
