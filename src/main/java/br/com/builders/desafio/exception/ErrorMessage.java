/*
 * Copyright 2018 Builders
 *************************************************************
 *Nome     : ErrorMessage.java
 *Autor    : Builders
 *Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
 *Empresa  : Platform Builders
 *************************************************************
 */
package br.com.builders.desafio.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;
import lombok.Value;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * An DTO that represents when an error occurred.
 *
 * @author Felipe Tasca
 */
@Value
@JsonInclude(Include.NON_EMPTY)
public class ErrorMessage implements Serializable {

    private int code;

    private String message;

    private List<String> errors;

    public ErrorMessage(final int code, final String message) {
        this.code = code;
        this.message = message;
        this.errors = Collections.emptyList();
    }

    public ErrorMessage(final int code, final String message, final List<String> errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

}
