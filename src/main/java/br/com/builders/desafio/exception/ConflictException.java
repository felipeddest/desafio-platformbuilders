package br.com.builders.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An exception that represents when an request conflict with the server state.
 * <p/>
 * Throwing that exception on a request handling will return a the HTTP CONFLICT 409 finishing the request.
 *
 * @author Felipe Tasca
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message);
    }

}
