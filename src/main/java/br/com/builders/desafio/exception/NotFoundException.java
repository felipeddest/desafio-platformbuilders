/*
 * Copyright 2018 Builders
 *************************************************************
 *Nome     : NotFoundException.java
 *Autor    : Builders
 *Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
 *Empresa  : Platform Builders
 *************************************************************
 */
package br.com.builders.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An exception that represents when the server cannot find the requested state.
 * <p/>
 * Throwing that exception on a request handling will return a the HTTP NOT FOUND 404 finishing the request.
 *
 * @author Felipe Tasca
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }

}

