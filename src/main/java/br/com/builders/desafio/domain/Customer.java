package br.com.builders.desafio.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * An entity that represents a Customer.
 *
 * @author Felipe Tasca
 */
@Data
@Builder
@Document(collection = "felipe_tasca_customer")
public class Customer {

    @Id
    private String id;
    private String crmId;
    private String baseUrl;
    private String name;
    @Indexed(unique = true)
    private String login;

}
