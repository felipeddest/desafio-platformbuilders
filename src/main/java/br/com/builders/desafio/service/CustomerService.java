package br.com.builders.desafio.service;

import br.com.builders.desafio.domain.Customer;
import br.com.builders.desafio.exception.ConflictException;
import br.com.builders.desafio.exception.NotFoundException;
import br.com.builders.desafio.repository.CustomerRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.UUID.randomUUID;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Update.fromDBObject;

/**
 * The class <code>CustomerService</code> provide services to handle Customer needs as save, retrieve, update or delete.
 *
 * @author Felipe Tasca
 */
@Service
@AllArgsConstructor
public class CustomerService {

    private static final String CUSTOMER_NOT_FOUND_MSG = "Customer with this ID not found";

    private final CustomerRepository repository;
    private final MongoTemplate mongoTemplate;
    private final MongoConverter mongoConverter;

    public List<Customer> getAll() {
        return repository.findAll();
    }

    public Customer createCustomer(final Customer customer) {
        if (mongoTemplate.exists(new Query(where("login").is(customer.getLogin())), Customer.class)) {
            throw new ConflictException("An existing item(login) already exists");
        }
        customer.setId(randomUUID().toString());
        return repository.save(customer);
    }

    public Customer getCustomer(final String id) {
        final Customer customer = repository.findOne(id);
        if (customer == null) {
            throw new NotFoundException(CUSTOMER_NOT_FOUND_MSG);
        }
        return customer;
    }

    public void updateCustomer(final String id, final Customer customer) {
        validateIfExists(id);
        final Query query = new Query(where("id").is(id));
        customer.setId(id);
        final Update update = fromDBObject(new BasicDBObject("$set", getDbObject(customer)));
        mongoTemplate.findAndModify(query, update, Customer.class);
    }

    public void deleteCustomer(final String id) {
        validateIfExists(id);
        repository.delete(id);
    }

    private void validateIfExists(final String id) {
        if (!mongoTemplate.exists(new Query(where("id").is(id)), Customer.class)) {
            throw new NotFoundException(CUSTOMER_NOT_FOUND_MSG);
        }
    }

    private DBObject getDbObject(final Object object) {
        final BasicDBObject basicDBObject = new BasicDBObject();
        mongoConverter.write(object, basicDBObject);
        return basicDBObject;
    }
}
