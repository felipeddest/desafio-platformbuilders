package br.com.builders.desafio.repository;

import br.com.builders.desafio.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

/**
 * A customer repository for MongoDB.
 *
 * @author Felipe Tasca
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {

}
