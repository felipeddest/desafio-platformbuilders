package br.com.builders.desafio.rest;

import br.com.builders.desafio.domain.Customer;
import br.com.builders.desafio.dto.request.CreateOrReplaceCustomerRequest;
import br.com.builders.desafio.dto.request.CustomerRequest;
import br.com.builders.desafio.dto.request.ModifyCustomerRequest;
import br.com.builders.desafio.dto.response.CustomerResponse;
import br.com.builders.desafio.dto.response.DetailedCustomerResponse;
import br.com.builders.desafio.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

/**
 * A controller for Customer that provide REST services.
 *
 * @author Felipe Tasca
 */
@RestController
@AllArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {

    private final CustomerService service;

    @PostMapping("/customers")
    public ResponseEntity<?> createCustomer(@RequestBody @Valid final CreateOrReplaceCustomerRequest request) {
        final URI location = fromCurrentRequest().path("/{id}")
                .buildAndExpand(service.createCustomer(adapt(request)).getId()).toUri();
        return ResponseEntity.created(location).body("{}");
    }

    @GetMapping("/customers")
    public List<CustomerResponse> listCustomers() {
        return service.getAll()
                .stream()
                .map(customer -> CustomerResponse.builder()
                        .name(customer.getName())
                        .baseUrl(customer.getBaseUrl())
                        .crmId(customer.getCrmId())
                        .build())
                .collect(toList());
    }

    @GetMapping("/customers/{customerId}")
    public DetailedCustomerResponse getCustomer(@PathVariable("customerId") final String id) {
        final Customer customer = service.getCustomer(id);
        return DetailedCustomerResponse.builder()
                .id(customer.getId())
                .baseUrl(customer.getBaseUrl())
                .crmId(customer.getCrmId())
                .login(customer.getLogin())
                .name(customer.getName())
                .build();
    }

    @PutMapping("/customers/{customerId}")
    public ResponseEntity<?> replaceCustomer(@PathVariable("customerId") final String id,
                                             @RequestBody @Valid final CreateOrReplaceCustomerRequest request) {
        service.updateCustomer(id, adapt(request));
        return ResponseEntity.ok("{}");
    }

    @PatchMapping("/customers/{customerId}")
    public ResponseEntity<?> modifyCustomer(@PathVariable("customerId") final String id,
                                            @RequestBody @Valid final ModifyCustomerRequest request) {
        service.updateCustomer(id, adapt(request));
        return ResponseEntity.ok("{}");
    }

    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("customerId") final String id) {
        service.deleteCustomer(id);
        return ResponseEntity.ok("{}");
    }

    private Customer adapt(final CustomerRequest request) {
        return Customer.builder()
                .name(request.getName())
                .crmId(request.getCrmId())
                .baseUrl(request.getBaseUrl())
                .login(request.getLogin())
                .build();
    }

}
