package br.com.builders.desafio.dto.response;

import lombok.Builder;
import lombok.Value;

/**
 * A basic response of customer.
 *
 * @author Felipe Tasca
 */
@Value
@Builder
public class CustomerResponse {

    private String crmId;

    private String baseUrl;

    private String name;

}
