package br.com.builders.desafio.dto.response;

import lombok.Builder;
import lombok.Value;

/**
 * A complete response of customer.
 *
 * @author Felipe Tasca
 */
@Value
@Builder
public class DetailedCustomerResponse {

    private String id;

    private String crmId;

    private String baseUrl;

    private String name;

    private String login;

}
