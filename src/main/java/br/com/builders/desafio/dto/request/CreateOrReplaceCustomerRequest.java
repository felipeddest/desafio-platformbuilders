package br.com.builders.desafio.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * The class <code>CreateOrReplaceCustomerRequest</code> is used as a DTO
 * that validate all fields to create and replace the customer.
 *
 * @author Felipe Tasca
 */
@Data
@NoArgsConstructor
public class CreateOrReplaceCustomerRequest implements CustomerRequest {

    @NotEmpty
    private String crmId;

    @NotEmpty
    private String baseUrl;

    @NotEmpty
    private String name;

    @NotEmpty
    @Email
    private String login;

}
