package br.com.builders.desafio.dto.request;

/**
 * This interface has the objective of centralize all equally methods of CustomerRequests,
 * turning easy the adaptation to the <code>Customer</code> entity.
 *
 * @author Felipe Tasca
 */
public interface CustomerRequest {

    String getCrmId();

    String getBaseUrl();

    String getName();

    String getLogin();

}
