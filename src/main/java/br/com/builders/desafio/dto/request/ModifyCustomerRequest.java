package br.com.builders.desafio.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

/**
 * The class <code>ModifyCustomerRequest</code> is used as a DTO to modify the customer data.
 * Does not require all fields.
 *
 * @author Felipe Tasca
 */
@Data
@NoArgsConstructor
public class ModifyCustomerRequest implements CustomerRequest {

    private String crmId;

    private String baseUrl;

    private String name;

    @Email
    private String login;

}
